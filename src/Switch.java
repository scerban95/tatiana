public class Switch {
    public static void main(String args[]) {
int day= 5;
        switch(day) {
            case 1:
                System.out.println("Luni");
                break;
            case 2:
                System.out.println("Marti");
                break;
            case 3:
                System.out.println("Miercuri");
                break;
            case 4:
                System.out.println("Joi");
                break;
            case 5:
                System.out.println("Azi e Vinerea!");
                break;
            case 6:
                System.out.println("Simbata");
                break;
            case 7:
                System.out.println("Duminica");
                break;
            default:
                System.out.println("Nici una din cele 7 zile");
        }

    }
}
